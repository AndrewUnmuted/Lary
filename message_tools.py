import getpass
import pgpy
import pickle
import soundfile as sf


PGP_KEY_PATH = "/home/agrathwohl/.keys/akey.asc"  # Recipient's public key
PGPRIV_PATH = "/home/agrathwohl/.private/private.key"  # Your private key
MSG_FILENAME = "message.asc"
MSG_DATA = "_messagedata"


def encrypt_message(wavefile):
    data, samplerate = sf.read(wavefile)
    d = data.tolist()
    key, _ = pgpy.PGPKey.from_file(PGP_KEY_PATH)
    sec, _ = pgpy.PGPKey.from_file(PGPRIV_PATH)
    try:
        f = open(MSG_DATA, 'wb')
        pickle.dump(d, f)
    finally:
        f.close()
    # Write PGP-encrypted stream to disk
    file_message = pgpy.PGPMessage.new(MSG_DATA, file=True)
    assert sec.is_protected
    assert sec.is_unlocked is False
    print('Enter key passphrase to sign your message:  ')
    key_password = getpass.getpass()
    with sec.unlock(key_password):
        assert sec.is_unlocked
        file_message |= sec.sign(file_message)
        msgstr = str(file_message)
    print('We will write the communication to file:  ', MSG_FILENAME)
    msg = open(MSG_FILENAME, 'wt')
    msg.write(msgstr)
    msg.close()
    return msgstr  # ASCII Armored


def decrypt_message(pgp_msg):
    from receive_laryngitis import play_message
    import subprocess as sp
    import tempfile as tf
    print("We're gonna decrypt some secrets.")
    # create a temporary file using a context manager
    try:
        fgpg = open('encrypted.message', 'wb')
        fgpg.write(bytes(pgp_msg + "\n", "ascii"))
    except IOError:
        print("ERROR: The file could not be opened.")
        return
    else:
        gpg_cmd = ['gpg', '-d', 'encrypted.message']
        try:
            fp = open(tf.NamedTemporaryFile().name, 'wb')
            fp_msg = sp.check_output(gpg_cmd)
            fp.write(bytes(fp_msg))
            fp.seek(0)
        except:
            print("An error occurred!")
        else:
            print("Beginning message playback.")
            play_proc = play_message(fp.name)
            print("Find your message at file:  ", play_proc)
        finally:
            fp.close()
    finally:
        fgpg.close()
