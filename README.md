# Lary

Encrypt and sign your audio messages via PGP. Enables safe transmission of voice data over clearnet.

## Dependencies

This here is a Python 3 application. So, make sure you're running that on your system. Then, get the module dependencies:

`pip install numpy pysoundfile pgpy`

## Status

This code is really just a proof of concept. Because I am not widely experienced in cryptography, I would gladly accept anyone's offer for assistance in developing this further along into maturity. The free and open web needs a safe and really instantaneous means for voice communication, so I will take whatever help I can get!

## Getting Started

Open up the `message_tools.py` file, and edit the global values at the top.

```python
PGP_KEY_PATH = "/home/user/.keys/key.asc"        # Recipient's public key
PGPRIV_PATH = "/home/user/.private/private.key"  # Your private key
```

Next, open your preferred python3 shell and import the encrypt and decrypt methods. Then you're ready to go.

```python
>>> from message_tools import decrypt_message, encrypt_message
>>> e = encrypt_message('demo.wav')
Enter key passphrase to sign your message:  
Password: 
We will write the communication to file:   message.asc
```

The file `message.asc` will contain an ASCII-Armored PGP string. This can be sent to a recipient via email or text-based communications systems.

Decrypting a message is as easy as:

```python
>>> decrypt_message(e)
gpg: Signature made Sun 31 Jan 2016 12:53:05 PM EST using RSA key ID 8527ABE5
gpg: Good signature from "PRIVATEPERSON <private@persons.org>" [ultimate]
Find your message at file:   _decr.wav
```

## Next

* Make an example contacts YAML file and implement its functionality
* Devise some automated tests for both performance and security
* Build audio recording helper methods that save audio streams to compressed OPUS files
* Pack PGP strings into msgpack for network transmission
* Develop a Redis-based message handling sever